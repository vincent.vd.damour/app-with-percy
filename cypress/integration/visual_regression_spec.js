describe('Visual Regression', () => {
  it('Login', () => {
    cy.visit('https://percy-266db.web.app/auth/login')
    
    cy.percySnapshot('Login');
  })

  it('Dashboard', () => {
    cy.visit('https://percy-266db.web.app/auth/login')
    cy.get('#signin').click()

    cy.percySnapshot('Dashboard');
  })

  it('Icons', () => {
    cy.visit('https://percy-266db.web.app/auth/login')
    cy.get('#signin').click()
    cy.get('.nav-item').eq(3).click()

    cy.percySnapshot('Icons');
  })

  it('Maps', () => {
    cy.visit('https://percy-266db.web.app/auth/login')
    cy.get('#signin').click()
    cy.get('.nav-item').eq(4).click()

    cy.percySnapshot('Maps');
  })

  it('User Profile', () => {
    cy.visit('https://percy-266db.web.app/auth/login')
    cy.get('#signin').click()
    cy.get('.nav-item').eq(5).click()

    cy.percySnapshot('User Profile');
  })

  it('Tables', () => {
    cy.visit('https://percy-266db.web.app/auth/login')
    cy.get('#signin').click()
    cy.get('.nav-item').eq(6).click()

    cy.percySnapshot('Tables');
  })

  it('Register', () => {
    cy.visit('https://percy-266db.web.app/auth/login')
    cy.get('#signin').click()
    cy.get('.nav-item').eq(8).click()

    cy.percySnapshot('Register');
  })
})